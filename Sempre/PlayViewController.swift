//
//  PlayViewController.swift
//  Sempre
//
//  Created by Yash Rajana on 2/22/19.
//  Copyright © 2019 Yash Rajana. All rights reserved.
//

import UIKit
import CoreData
import AVFoundation
class PlayViewController: UIViewController,  UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    @IBOutlet weak var musicImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    
    //musicImageView.image = newImage
    var audioPlayer = AVAudioPlayer()
    
    var timer = Timer()
    var timerIsRunning = false
    
    var pageMeasureNumber : [Int] = []
    var beatsPerMeasure : Int16 = 0
    var pageImages : [UIImage] = []
    var beatsPerMinute : Int16 = 0
    
    var currentPage = 0
    var counter = 0
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    
    
    var songName: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let sound = Bundle.main.path(forResource: "Click", ofType: "mp3")
        do{
            AVAudioPlayer()
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound!))
            
        } catch{
            print(error)
            
        }
    self.loadSongData()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func playButton(_ sender: Any) {
        var time_forPage1: Double
        time_forPage1 = 0//what needs to go here? I guessed and put 0
        if timerIsRunning == false {
            //start playing
            timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(PlayViewController.playMetSound), userInfo: nil, repeats: true)
            playButton.setTitle("Stop", for: .normal)
            
        }else{
            //stop playing
            timer.invalidate()
            playButton.setTitle("Start", for: .normal)
        }
        
        timerIsRunning = !timerIsRunning

    }
    
    @objc func playMetSound() {
        audioPlayer.play()
        counter = counter + 1
        if counter > (pageMeasureNumber[currentPage] * Int(beatsPerMeasure)){
            currentPage = currentPage + 1
                musicImageView.image = pageImages[currentPage]
            counter = 0
            
        }
        
    }
    
    func loadSongData(){
        do{
            
            let fetchRequest : NSFetchRequest<Song> = Song.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "title == %@, songName")
            let songs = try context.fetch(fetchRequest) as! [Song]
            let song = songs.first
            
            self.beatsPerMeasure = song!.beatsPerMeasure
            self.beatsPerMinute = song!.beatsPerMinute
            self.pageMeasureNumber = song!.pageMeasureNumbers
            self.pageImages = song!.pageImages
            
            if counter > (pageMeasureNumber[currentPage]*Int(beatsPerMeasure)){
                
           //flips page
                currentPage = currentPage + 1
                    counter = 0
                musicImageView.image = pageImages[currentPage]
                
                
                
                
            }
        }catch{
            print (error as NSError)
        }
        
        
        
    }
    
    
}

